# Prosjekt - PROG2053

## Installation notes:
- Install nodeJs
- Run "npm i" in both the server and client folders of the project.
- Run "npm install mysql express cors bcryptjs passport passport-local method-override express-session express-flash" in server folder.
- Run "npm install --save-dev nodemon dotenv" in server folder.
- Run "npm install --save @vaadin/router" in client folder.
- In server folder add file .env, in env file add "SESSION_SECRET = passwordsecret".
- While in the root folder of the project: "docker-compose up -d" - this will take a long time the first time you do it.
- Are you getting an error after doing "docker-compose up -d", saying "[...] Filesharing has been cancelled"?
-> Go to Docker for Windows app (or similar) -> Settings -> Resources -> File sharing -> Add all your drives (or play around with figuring out what exactly you need).
- Are you getting an error saying "npm ERR! ch() never called"?
-> Delete "package-lock.json" from the client directory, then build the client again using "docker-compose build client"

Want to reset your containers and volumes fully?
- "docker system prune -a --volumes"

Want to get in to a container for some reason?
- "docker-compose exec <containername> bash"

## Group members:     
- Fabian Kongelf
- Yan Senko
- Hamza Azim
- Olav Andreas Strandjord
- Petter Jacob Brautaset

## Setup:
- docker-compose up -d   
- webpage is hosted on: localhost:8080

## information:
The available function of the webpage is dependent on logged in users userType, alternetivly not logged inn.
A users who is not logged in cannot post new posts or comments, only view existing posts/comments or register/login.
Regular users can post new posts and comments, and gain access to profile page, which lists posts/comments logged in users has made, users can also request moderator promotion.
As we have no realistic and long term(longer then a server reboot) storage of blocked posts/comments moderators have the same abilitys as regular users.
Administators can do all the same as users/moderators and delete posts.
All users(and not logged in) can search after posts, the search is compared to posts titles.
To change roles on users from user to admins it has to be done on localhost:8082 (username=admin, password=password) and server has to be rebooted