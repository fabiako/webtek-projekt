import { Router } from '@vaadin/router';
import './src/nav-sm-element.js';
import './src/post-element.js';
import './src/login-element.js';
import './src/reg-element.js';
import './src/nuPost-element.js';
import './src/search-respones.js';
import './src/profil-element';
import './src/moderatorApplication-element.js'

const outlet = document.getElementById('outlet');
const router = new Router(outlet);

router.setRoutes([
    { path: '/', animate: true, children: [
        { path: '', component: 'post-element' },
        { path: '/nav-sm', component: 'nav-sm-element' },
        { path: '/search', component: 'search-element' },
        { path: '/login', component: 'login-element' },
        { path: '/register', component: 'reg-element' },
        { path: '/new-post', component: 'nupost-element' },
        { path: '/profil', component: 'profile-element' },
        { path: '/modreq', component: 'modreq-element'},
        { path: '(.*)', redirect: '/' }    // fallback??
    ]}   
]);