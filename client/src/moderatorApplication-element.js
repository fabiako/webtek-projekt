import { LitElement, html, css } from '../node_modules/lit-element/lit-element';
import { costumContainer, input } from './styles.js';

export class modreq extends LitElement {
   constructor() {
      super();
  }

  static get styles() {
   return [
       costumContainer,
       input,
       css`
    :host {
        display: block;
    }
         form {
            width: 500px;
            transform: translateY(-12vh);
      }

      input, #post {
            background: rgb(25,25,25) !important;
      }

      #post {
            height: 100px;
            border: none !important;
            color: white !important;
      }

      button {
            height: 50px;
      }
    `,
   ]
}

    render() {
        return html`
        <div class="container costum-container">
            <div class="row costum-container justify-content-center align-items-center" style="background: rgba(0,0,0,.9)">
               <form action=”/” @submit="mailto:bartell.martine@example.com" method=”POST” name="ModForm">
                  <h3 class="text-white">New Post :</h3>
                           <div id="formTitle" class="form-group mb-3">
                              <label for="title">Mail</label>
                              <input type="text" name="title" class="form-control" placeholder="Mail:" required>
                           </div>
                           <div id="formText" class="form-group mb-4">
                              <label for="content">Why do you want to be a Moderator?</label>
                              <textarea name="content" id="post" class="form-control" required></textarea>
                              <!-- <input type="text" name="content" id ="post" class="align-self-top" required> -->
                           </div>
                           <div id="formSubmit" class="form-group">
                              <button type="submit" class="btn btn-primary col-12">Request!</button>
                     </div>
               </form>
            </div>
         </div>
        `;
    }
}
customElements.define('modreq-element', modreq);
