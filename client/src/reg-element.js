import { LitElement, html, css } from 'lit-element';
import { costumContainer, input } from './styles.js';

export class reg extends LitElement {
    constructor() {
        super();
    }

    static get styles() {
        return [
            costumContainer,
            input,
            css`
            form {
                width: 500px;
                transform: translateY(-8vh);
            }
            
            input {
                background: rgb(25, 25, 25) !important;
                box-shadow: none !important;
                font-size: 18px !important;
            }
    
            button {
                height: 50px;
            }
            `,
        ]
    }

    render() {
        return html`
            <div class="container costum-container">
                <div class="row costum-container align-items-center justify-content-center" style="background: rgba(0,0,0,.9)">
                    <form action="${window.MyAppGlobals.serverURL}register" method="POST">
                        <h3 class="text-white mb-4">Register here.</h3>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control text-muted" name="email" aria-describedby="emailHelp" placeholder="Elon@SpaceX.com">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control text-muted" name="password" placeholder="************">
                        </div>
                        <div class="form-group mb-4">
                            <label for="rpassword">Repeat Password</label>
                            <input type="password" class="form-control text-muted" name="rpassword" placeholder="************">
                        </div>
                        <div class="form-group mb-1">
                            <button type="submit" class="btn btn-primary col-12">Register</button> 
                        </div>
                        <div class="row justify-content-center pt-0">
                            <div class="col-sm-auto">
                                <a class="text-secondary" href="/login">Already a user? Login here.</a>
                            </div>
                        </div>
                    </form>
                </div>     
            </div>
        `;
    }
}
customElements.define('reg-element', reg);