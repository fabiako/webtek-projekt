import { LitElement, html, css } from 'lit-element';
import { costumContainer, input } from './styles.js';

export class navarea extends LitElement {
    static get properties() {
        return {
            profpost: { type: String },
            logreg: { type: String }
        }
    }

    constructor() {
        super();
        this.profpost = 'd-inline';
        this.logreg = 'd-inline';
    }

    static get styles() {
        return [
            costumContainer,
            input,
            css`
                input {
                    background: rgba(0,0,0,0) !important;
                    box-shadow: none !important;
                    width: 240px !important;
                    transform: translateX(-8px) !important;
                    font-size: 22px !important;
                }
            `,
        ]
    }

    render() {
        return html`
            <div class="navbar navbar-dark costum-container">
                <div class="costum-container container-fluid align-items-center justify-content-center" style="background: rgba(0,0,0,.9)">
                    <ul class="navbar-nav flex-column" style="transform: translateY(-10vh)">
                        <li class="h2 nav-link active">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="h2 nav-link ${this.logreg}">
                            <a class="nav-link" href="/login">Login</a>
                        </li>
                        <li class="h2 nav-link ${this.logreg}">
                            <a class="nav-link" href="/register">Register</a>
                        </li>
                        <li class="h2 nav-link ${this.profpost}">
                            <a class="nav-link" href="/profil">Profil</a>
                        </li>
                        <li class="h2 nav-link ${this.profpost}">
                            <a class="nav-link" href="/new-post">New Post</a>
                        </li>
                        <li class="nav-link">
                            <form class="form-inline my-lg-0" action="/search">
                                <input class="form-control" type="search" name="search" id="search" placeholder="Search..." title="Press enter to search">
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        `;
    }
    
    firstUpdated() {
        fetch('http://localhost:8081/getUserType')
        .then(res => { return res.json() }).then(response => {
            if (response != 'guest') {
                this.logreg = 'd-none';
                this.profpost = 'd-inline'
            } else {
                this.profpost = 'd-none';
                this.logreg = 'd-inline';
            }
        }).catch(err => {console.log(err)});
    }
}
customElements.define('nav-sm-element', navarea);