import { LitElement, html, css } from 'lit-element';
import { costumContainer } from './styles.js';

class SearchResponse extends LitElement {
    static get properties() {
        return {
            threads: { type: Array },
            comments: { type: Array },
            admin: { type: Boolean },
            loggedIn: { type: Boolean },
            lenghtOfSearch: { type: String }
        }
    }

    constructor() {
        super();
        this.threads = [];
        this.comments = [];
        this.admin = false;
        this.loggedIn = false;
        this.lenghtOfSearch = 0;
        this.initializePosts();
    }

    static get styles() {
        return [
            costumContainer,        
            css`
                .costum-container {
                    background: rgba(0,0,0,.9);
                    height: 0 !important;
                    min-height: 100vh;
                }
                
                .thread-comments {
                    display: grid;
                    grid-template-columns: auto minmax(0, 1200px) auto;
                    transform: translateY(60px);
                }

                .search-container {
                    transform: translateY(60px);
                }

                .costum-bg {
                    background: rgb(30,30,30);
                }

                .costum-btn {
                    box-shadow: none !important;
                }

                .content-comments {
                    max-height: 0;
                    transition: max-height .2s ease-in-out;
                    overflow: hidden;
                }

                .grid {
                    display: grid;
                    grid-template-columns: 1fr 30px;
                }
            `]}

    render() {
        return html`
            <div class="costum-container">
                <div class="thread-comments">
                    <div></div>
                    <div>
                        <div class="ml-2">
                            <h2 class="h2">Search gave ${this.lenghtOfSearch} posts </h2>
                        </div>
                        <div class="accordion m-2" id="accordionExample">
                            ${this.threads.map(thread => html `
                                <p class="lead mb-0 ml-1">${thread.user}</p>
                                <div class="card costum-bg mb-2">
                                    <div class="card-header grid" id="headingOne">
                                        <h1 class="mb-0">
                                            <button class="btn btn-block costum-btn text-left text-light" value="${thread.pid}"  id="false" @click="${this.openClose}">
                                                ${thread.title}
                                            </button>
                                        </h1>
                                        <button class="btn text-muted costum-pos" value="${thread.pid}" @click="${this.yeetPost}" title="Remove this post" style="display: ${this.admin ? 'block' : 'none'}">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                            </svg>
                                        </button>
                                    </div>

                                    <div id="${thread.pid}" class="content-comments">
                                        <div class="card-body">
                                            <p class="text-light ml-1 mr-1">${thread.content}</p>
                                            <hr class="mt-1 mb-3 bg-secondary">
                                            <div class="container-fluid">
                                                ${this.comments.map(comment => {
                                                    if (comment.post == thread.pid) { return html `
                                                        <div class="row align-items-center">
                                                            <div class="col-3 lead">${comment.user}</div>
                                                            <div class="col-9 text-light">${comment.comment}</div>
                                                        </div>    
                                                    `}
                                                })}  
                                            </div>

                                            ${this.loggedIn ? html`
                                                <form class="mt-3 mx-1" action="${window.MyAppGlobals.serverURL}nuComment" method="POST">
                                                    <div class="row lead ml-1">New Comment</div>
                                                    <div class="row">
                                                        <div class="col-9">
                                                            <input type="input" class="form-control" id="comment" name="comment" title="comment content" >
                                                        </div>
                                                        <div class="col-3">
                                                            <button type="submit" name="submit" value="${thread.pid}" class="btn btn-primary col-12">Post</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            ` : ''}
                                        </div>
                                    </div>
                                </div>
                            `)}
                        </div>
                    </div>
                    <div></div>
                </div>
            </div>
        `;
    }
    // opens or closes a thread, based on buttons attribute: title
    openClose(e) {
        var cc = this.shadowRoot.getElementById(e.target.value);
        if (e.target.id == "false") {
            cc.style.maxHeight = "70vh";
            setTimeout(() => { cc.style.overflow = "auto" }, 300);
            e.target.id = "true";
        } 
        else {
            cc.style.maxHeight = "0";
            cc.style.overflow = "hidden";
            e.target.id = "false";
        }
    }

    firstUpdated() {
        fetch('http://localhost:8081/getUserType')
        .then(res => { return res.json() }).then(response => {
            if (response != 'guest') {
                this.loggedIn = true;
                if (response != 'admin') {
                    this.admin = false;
                } else {
                    this.admin = true;
                }
            } else {
                this.loggedIn = false;
                this.admin = false;
            }
        }).catch(err => {console.log(err)});
    }

    initializePosts() {
        let url = location.href;
        let start = url.indexOf('=');
        let end = url.length;
        let search = url.substring((start+1), end);
        search = search.replace('+', ' ');

        fetch('http://localhost:8081/search', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({'search': search})
        }).then(res => { return res.json() }).then(data => {
            if (data != null) {
                this.threads = data[0];
                this.comments = data[1]; 
            }
            this.lenghtOfSearch = data[0].length;
        }).catch(err => {console.log(err)});
    }
}
customElements.define('search-element', SearchResponse);