import { LitElement, html, css } from 'lit-element';
import { costumContainer, input } from './styles.js';

export class NuPost extends LitElement {
    constructor() {
        super();
    }

    static get styles() {
        return [
            costumContainer,
            input,
            css`
                form {
                    width: 500px;
                    transform: translateY(-12vh);
                }
            
                input, #post {
                    background: rgb(25,25,25) !important;
                }

                #post {
                    height: 100px;
                    border: none !important;
                    color: white !important;
                }

                button {
                    height: 50px;
                }
            `,
        ]
    }

    render(){
        return html`
            <div class="container costum-container">
                <div class="row costum-container justify-content-center align-items-center" style="background: rgba(0,0,0,.9)">
                    <form action="${window.MyAppGlobals.serverURL}makePost" method="POST">
                        <h3 class="text-white">New Post :</h3>
                        <div id="formTitle" class="form-group mb-3">
                            <label for="title">Subject</label>
                            <input type="text" name="title" class="form-control" placeholder="Subject:" required>
                        </div>
                        <div id="formText" class="form-group mb-4">
                            <label for="content">Content:</label>
                            <textarea name="content" id="post" class="form-control" required></textarea>
                            <!-- <input type="text" name="content" id ="post" class="align-self-top" required> -->
                        </div>
                        <div id="formSubmit" class="form-group">
                            <button type="submit" class="btn btn-primary col-12">Post!</button>
                        </div>
                    </form>
                </div>
            </div>
        `;
    }
}
customElements.define('nupost-element', NuPost);  