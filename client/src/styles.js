import { css } from 'lit-element';

export const costumContainer = css`
    .costum-container {
        margin: 0;
        padding: 0;
        width: 100vw;
        height: 100vh;
    }
`;

export const input = css`
    input {
        border: none !important;
        color: white !important;
    }

    ::placeholder {
        color: rgb(140,140,140) !important;
    }
`;