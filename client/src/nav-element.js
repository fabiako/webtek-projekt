import { LitElement, html, css } from 'lit-element';
import { input } from './styles.js';

export class Nav extends LitElement {
    static get properties() {
        return {
            profpost: { type: String },
            logreg: { type: String },
            navSmPath: { type: String },
            prevPath: { type: String }
        }
    }

    constructor() {
        super();
        this.profpost = 'd-inline';
        this.logreg = 'd-inline';
        this.navSmPath = '/nav-sm';
        this.prevPath;
    }

    static get styles() {
        return [
            input,
            css`
                .navbar {
                    z-index: 1000;
                    background: rgba(255,255,255,.05) !important;
                }

                input {
                    background: rgb(65,65,70) !important;
                    width: 300px !important;
                }

                @media screen and (min-width: 1000px) {
                    #large-nav { display: contents !important; }
                    #small-nav { display: none !important; }
                }

                @media screen and (max-width: 1000px) {
                    #large-nav { display: none !important; }
                    #small-nav { display: contents !important; }
                }
            `,
        ]
    }

    render() {
        return html`
                    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                        <a href="/" class="navbar-brand">
                            <img src="logo1.png" width="30" height="30" class="d-inline-block align-top" alt="Logo" loading="lazy">
                            5Chan
                        </a>

                        <!--- navbar for large screens --->
                        <div class="collapse navbar-collapse" id="large-nav">
                            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                                <li class="nav-item active">
                                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item mt-2 ${this.logreg}">
                                    <a class="nav-link d-inline" href="/login">Login</a>
                                    /
                                    <a class="nav-link d-inline" href="/register">Register</a>                   
                                </li>
                                <li class="nav-item mt-2 ${this.profpost}">
                                    <a class="nav-link d-inline" href="/profil">Profil</a> 
                                    / 
                                    <a class="nav-link d-inline" href="/new-post">New Post</a>                       
                                </li>
                            </ul>
                            <form class="form-inline my-2 my-lg-0" action="/search">
                                <input class="form-control mr-sm-2" type="search" name="search" placeholder="Search">
                                <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </div>
                        
                        <!--- navbar for small screens --->
                        <div class="navbar" id="small-nav">
                            <div class="ml-auto mr-2">
                                <form action="${this.navSmPath}">
                                    <button class="navbar-toggler" type="submit" @click="${() => this.toggleNavPage()}">
                                        <span class="navbar-toggler-icon"></span>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </nav>
                `;        
    }

    firstUpdated() {
        fetch('http://localhost:8081/getUserType')
        .then(res => { return res.json() }).then(response => {
            console.log(response);
            if (response != 'guest') {
                this.logreg = 'd-none';
                this.profpost = 'd-inline'
            } else {
                this.profpost = 'd-none';
                this.logreg = 'd-inline';
            }
        }).catch(err => {console.log(err)});
    }

    // display small nav
    toggleNavPage() {
        if (location.pathname != "/nav-sm") {
            this.navSmPath = "/nav-sm";
            this.prevPath = location.pathname;
        }
        else {
            this.navSmPath = this.prevPath;
        }
    }

    search() {
        let search = this.shadowRoot.getElementById("search").value;
        window.MyAppGlobals.search = search;
        window.location.href = 'localhost:8080/search';
    }
}
customElements.define('nav-element', Nav);