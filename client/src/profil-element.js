import { LitElement, html, css } from 'lit-element';
import { costumContainer, input } from './styles.js';

export class Profil extends LitElement {  
    static get properties() {
        return {
            threads: { type: Array },
            comments: { type: Array },
            user: { type: Object },
            commentsActive: { type: String },
            postsActive: { type: String }
        }
    }

    constructor() {
        super();
        this.threads = [];
        this.comments = [];
        this.user = {};
        this.commentsActive = "display: none";
        this.postsActive = "display: block";
        this.initPosts();
    }

    static get styles() {
        return [
            costumContainer,
            input,
            css`                
                .thread-comments {
                    display: grid;
                    grid-template-columns: 0 minmax(0, 1200px) auto;
                }

                .costum-bg {
                    background: rgb(60,60,60);
                }

                .costum-btn {
                    box-shadow: none !important;
                }

                .content-comments {
                    max-height: 0;
                    transition: max-height .2s ease-in-out;
                    overflow: hidden;
                }
                .grid {
                    transform: translateY(60px);
                    display: grid;
                    grid-template-columns: 1fr 2fr;
                }
            `,
        ]
    }

    render() {
        return html`
            <div class="costume-container">
                <div class="grid">
                    <div class="m-2" style="posistion: fixed;">   
                        <ul class="list-group costum-bg">
                            <li class="list-group-item">
                                <!-- img of user -->
                                <img class="img-circle img-responsive img-bordered-primary img-fluid" src="https://c8.alamy.com/comp/P9MYWR/man-avatar-profile-P9MYWR.jpg" alt="User">
                            </li>
                            <li class="list-group-item">
                                <h2 class="h4 text-capitalize"> ${this.user.email}</h2>
                                <p class="text-muted text-capitalize"> ${this.user.userType} </p>
                            </li>
                            <li class="list-group-item">
                                <a href="/modreq" class="btn btn-success col-12">Become moderator</a>
                            </li>
                            <li class="list-group-item">
                                <a href="#" class="btn btn-primary col-12">Logout</a>
                            </li>
                        </ul>
                    </div>

                    <div class="m-2">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link h3" id="posts" @click="${this.postOrComment}">Posts</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link h3" id="comments" @click="${this.postOrComment}">Comments</a>
                            </li>
                        </ul>

                        <div class="thread-comments" style="${this.postsActive}">
                            <div></div>
                            ${this.threads.map(thread => {
                                if (thread.user == this.user.email) { return html`
                                    <div class="accordion m-2" id="accordionExample">
                                        <div class="card costum-bg mb-2">
                                            <div class="card-header" id="headingOne">
                                                <h1 class="mb-0">
                                                    <button class="btn btn-block costum-btn text-left text-light" value="${thread.pid}"  id="false" @click="${this.openClose}">
                                                        ${thread.title}
                                                    </button>
                                                </h1>
                                            </div>

                                            <div id="${thread.pid}" class="content-comments">
                                                <div class="card-body">
                                                    <p class="text-light mx-1">${thread.content}</p>
                                                    <hr class="mt-1 mb-3 bg-secondary">
                                                    <div class="container-fluid">
                                                        ${this.comments.map(comment => {
                                                            if (comment.post == thread.pid) { return html`
                                                                <div class="row align-items-center">
                                                                    <div class="col-3 lead">${comment.user}</div>
                                                                    <div class="col-9 text-light">${comment.comment}</div>
                                                                </div>
                                                            `}
                                                        })}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                  
                                    </div>
                                `} 
                            })}
                            <div></div>
                        </div>

                        <div class="thread-comments" style="${this.commentsActive}">
                            <div></div>
                            <div class="card costum-bg"> <!-- fortsatt hvit bg -->
                            ${this.comments.map(comment => {
                                if (comment.user == this.user.email) { 
                                    let post = this.threads.find(thread => { return thread.pid == comment.post });
                                    let title = post.title;
                                    return html`
                                    <div class="card-body">
                                        <h5 class="card-title"> Comment from post: ${title} </h5>
                                        <p class="card-text"> ${comment.comment} </p>
                                    </div>
                                `}
                            })}
                            </div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
    }
    // opens or closes a thread, based on buttons attribute: href
    openClose(e) {
        var cc = this.shadowRoot.getElementById(e.target.value);
        if (e.target.id == "false") {
            cc.style.maxHeight = "70vh";
            setTimeout(() => { cc.style.overflow = "auto" }, 300);
            e.target.id = "true";
        }
        else {
            cc.style.maxHeight = "0";
            cc.style.overflow = "hidden";
            e.target.id = "false";
        }
    }

    postOrComment(e) {
        if (e.target.id == "posts") {
            this.postsActive = "display: block";
            this.commentsActive = "display: none";
        } else {
            this.postsActive = "display: none";
            this.commentsActive = "display: block";
        }
    }

    async initPosts() {
        this.user = await this.getUser();

        if (this.user != {}) {
            fetch('http://localhost:8081/postsAndComments')
            .then(res => { return res.json() }).then(data => {
                if (data != null) {
                    this.threads = data[0];
                    this.comments = data[1];
                }
            }).catch(err => {console.log(err)});
        }        
    }
    
    getUser() {
        return new Promise((resolve, reject) => {
            fetch('http://localhost:8081/currentUser')
            .then(res => { return res.json() }).then(data => {
                if (data != "ERR") {
                    resolve(data);
                } else { console.log("not logged in") }
            }).catch(err => { console.log(err) });
        });
    }
}

customElements.define('profile-element', Profil);