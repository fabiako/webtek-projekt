const mysql = require('mysql2');
const dbConnection = mysql.createPool({
    host     : 'localhost', // MYSQL host
    user     : 'root', // MYSQL brukernavn
    password : '', // MYSQL passord
    database : 'nodejs_login' // MYSQL databasenavn
}).promise();
module.exports = dbConnection;