"use strict";

import dotenv from 'dotenv';
if (process.env.NODE_ENV !== 'production') {
  dotenv.config();
}

import express from 'express';
import path from 'path';
import mysql from 'mysql';
import cors from 'cors';
import bodyParser from 'body-parser'
import passport from 'passport';
import methodOverride from 'method-override';
import session from 'express-session';
import flash from 'express-flash';
const app = express();
const PORT = 8081;
const INDEX = 'http://localhost:8080';

//global variabel for bruker
var usr = null;

// Hvilken port stuff kjører på
app.listen(PORT, () => {
  console.log('Running...');
});

// uses
app.use(cors({ origin : INDEX }));
app.use(express.static(path.resolve() + '/server'));
app.use(express.urlencoded({ extended : false }));
app.use(bodyParser.json());

app.use(flash());
app.use(session({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(methodOverride('_method'));

// Logg inn infoen til phpmyadmin
var db = mysql.createConnection({
  host: "db",
  user: "admin",
  password: "password",
  database: 'prog2053-proj',
  multipleStatements: true
});

db.connect((err) => {
  if (err) {
    throw err;
  }
  console.log("Connected!");
});

// route til hovedsiden (port 8081)
app.get('/', (req, res) => {
  res.send("Hello world");
});

// 8081/getUsers side, lister opp alle brukere
app.get('/getUsers', (req, res) => {
  db.query('SELECT * FROM users', function (err, result) {
    if (err) {
      console.log('Error in database operation, users');
    } else {
      res.end(JSON.stringify(result));
    }
  });
});

// post data to client
app.get('/getPosts', (req, res) => {
  db.query('SELECT * FROM posts', (err, result) => {
    if (err) {
      console.log("Error in database operation, posts");
    } else {
      res.end(JSON.stringify(result));
    }
  });
});

// post data to client
app.get('/getComments', (req, res) => {
  db.query('SELECT * FROM comments', (err, result) => {
    if (err) {
      console.log("Error in database operation, comments");
    } else {
      res.end(JSON.stringify(result));
    }
  });
});

// to display post and comments
import { postsAndComments } from './src/postsAndComments.js'; // muligens ikke best...
app.get('/postsAndComments', (req, res) => { 
  postsAndComments().then(result => {
    res.end(JSON.stringify(result));
  });
});

// mottar post data fra login
import { initialize } from './src/passport-config.js';
initialize(passport);
app.post('/login', checkNotAuth, 
  passport.authenticate('local'), (req, res) => {
    res.redirect( INDEX );
    console.log(req.user);
    usr = req.user;
  }
);

// register user
import { register } from './src/register.js';
app.post('/register', checkNotAuth, (req, res) => {
  register(req.body).then(result => {
    let sql = "INSERT INTO users(email, password, userType, picture) VALUES (?,?,?,?);";
    db.query(sql, result);
    res.redirect(INDEX + '/login');
  }).catch((err) => {
    console.log(err);
    res.redirect(INDEX + '/register');
  });
});

// search (from nav)
import { search } from './src/search.js';
import { getUsers } from './src/SQLquerys.js';
app.post('/search', (req, res) => {
  try {
    search(req.body.search).then(result => {
      res.setHeader('Access-Control-Allow-Origin', INDEX);
      res.end(JSON.stringify(result));
    });
  }
  catch {
    console.log("Failed to search");
  }
});


//Legger til ny post
app.post('/makePost', (req, res) => {
  var sql = "INSERT INTO posts(user, title, content) VALUES (?,?,?);";
  db.query(sql, [usr.uid, req.body.title, req.body.content]);
  console.log("New post: " + req.body.title + ", stating that " + req.body.content)  
  res.redirect(INDEX);
});

//sletter post
app.post('/yeetPost', (req, res) => {
  db.query(`DELETE FROM comments WHERE post = ${req.body.post};`)
  var sql = `DELETE FROM posts WHERE pid = ${req.body.post};`;
  db.query(sql);
  console.log("Post witd pid = " + req.body.post + " deleted.");
  res.redirect(INDEX);
});

//Legger til ny kommentar
app.post('/nuComment', (req, res) => {
  var sql = "INSERT INTO comments(post, user, comment) VALUES (?,?,?);";
  db.query(sql, [req.body.submit, usr.uid, req.body.comment]);
  console.log("New post: " + req.body.title + ", stating that " + req.body.content)  
  res.redirect(INDEX);
});

// get user type
app.get('/getUserType', (req, res) => {
  if (usr) {  
    res.end(JSON.stringify(usr.userType));
  } else {
    res.end(JSON.stringify("guest"));
  }
});

// get current user
app.get('/currentUser', (req, res) => {
  if (usr) {
    res.end(JSON.stringify(usr));
  } else {
    res.end(JSON.stringify("ERR"));
  }
})

// Get User profile
app.get('/profil', (req, res) => {
  if (req.session.uid)
  db.query('SELECT * FROM users', (err, result) => {
    if (err) {
      console.log("Error in database operation, userInfo");
    } else {
      res.end(JSON.stringify(result.uid));
    }
  });
});



app.delete('/logout', (req, res) => {
    req.logOut();
    res.redirect(INDEX);
});

function checkIfAuth(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect(INDEX + '/login');
}

function checkNotAuth(req, res, next) {
  if(req.isAuthenticated()) {
    return res.redirect(INDEX);
  }
  next();
}

