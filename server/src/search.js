import { postsAndComments } from './postsAndComments.js'; // all posts and comments

export function search(search) {
    return new Promise((resolve, reject) => {
        let resPost = [], resComments = [], promise, str;

        postsAndComments().then(result => {   // from /postsAndComments
            promise = new Promise((resolve, reject) => {  // limiting race condition
                result[0].forEach(post => {
                    str = post.title.toUpperCase();
                    if (str.indexOf(search.toUpperCase()) != -1) { // indexOf = -1 means that index is not part of string
                        resPost.push(post);
                    }
                });
                resolve();
            });
            promise.then(response => {  // wait for promise to finish
                result[1].forEach(comment => {
                    resPost.forEach(post => {
                        if (comment.post == post.pid) {
                            resComments.push(comment);
                        }
                    });
                });
                resolve([resPost, resComments]);
            });            
        });    
    });    
} 