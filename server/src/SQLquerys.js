import mysql from 'mysql';

var db = mysql.createConnection({
    host: "db",
    user: "admin",
    password: "password",
    database: 'prog2053-proj',
    multipleStatements: true
});

export function getUsers() {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM users', (err, result) => {
            if (err) {
            console.log("Error in database operation, posts");
            } else {
            resolve(result);
            }
        }); 
    });  
}
  
export function getPosts() {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM posts', (err, result) => {
            if (err) {
            console.log("Error in database operation, posts");
            } else {
            resolve(result);
            }
        }); 
    });
}
  
export function getComments() {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM comments', (err, result) => {
            if (err) {
            console.log("Error in database operation, posts");
            } else {
            resolve(result);
            }
        });  
    });
}