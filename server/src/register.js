import bcrypt from 'bcryptjs';
import { getUsers } from './SQLquerys.js';

export function register(user) {
    return new Promise((resolve, reject) => {
        let users, hashedPassword;

        Promise.allSettled([
            getUsers(),
            hashPassword(user.password)
        ]).then(results => {
            users = results[0].value;
            hashedPassword = results[1].value;

            if (users.find(aUser => aUser.email == user.email)) {
                reject("Email in use.");
            }

            if (user.password == user.rpassword) {
                resolve([user.email, hashedPassword, "user", null]);
            } else {
                reject("Passwords dont match.");
            }
        });
    });
}

function hashPassword(password) {
    return new Promise((resolve, reject) => {
        resolve(bcrypt.hash(password, 10));
    });
}