import { getUsers, getPosts, getComments } from './SQLquerys.js';

export function postsAndComments() {
    return new Promise((resolve, reject) => {
        let posts, comments, result = [];

        createArrays().then(respons => {
            posts = correctUsers(respons[2], respons[0]);
            comments = correctUsers(respons[2], respons[1]);

            Promise.allSettled([posts, comments]).then(respons => {
            respons.forEach(element => {
                result = [ ... result, element.value ];
            });
            resolve(result);
            });
        });
    })
    
}
  
function createArrays() {
    return new Promise((resolve, reject) => {
        let posts, comments, users, result = [];
      
        posts = getPosts();
        comments = getComments();
        users = getUsers();
  
        Promise.allSettled([posts, comments, users]).then(res => {
            res.forEach(element => {
            result = [ ... result, element.value ];
            });
            resolve(result);
        });
    });  
}
  
function correctUsers(users, array) {
    return new Promise((resolve, reject) => {
        for (let u = 0; users[u]; u++) {
            for (let a = 0; array[a]; a++) {
            if (array[a].user == users[u].uid) {
                array[a].user = users[u].email;
            }
            }
        }
        resolve(array);
    });  
}