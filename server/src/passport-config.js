import passportLocal from 'passport-local';
import bcrypt from 'bcryptjs';
import { getUsers } from './SQLquerys.js';
const LocalStrategy = passportLocal.Strategy;

export async function initialize(passport) {
  const authenticateUser = async (email, password, done) => {
    const users = await getUsers();
    const user = users.find(user => user.email === email);

    if(user == null) {
      return done(null, false, { message: "wrong email or password" });
    }
    
    try {
      if (await bcrypt.compare(password, user.password)) {
        return done(null, user);
      } else {
        return done(null, false, { message: "wrong email or password" });
      }
    } catch (err) {
      return done(err);
    }
  }

  passport.use(new LocalStrategy({ usernameField: 'email' }, authenticateUser));
  passport.serializeUser((user, done) => done(null, user.uid));
  passport.deserializeUser(async (id, done) => {
    const users = await getUsers();
    const user = users.find(user => user.uid === id);
    return done(null, user);
  });
}