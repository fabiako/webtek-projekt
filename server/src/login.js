import mysql from 'mysql';

var db = mysql.createConnection({
    host: "db",
    user: "admin",
    password: "password",
    database: 'prog2053-proj',
    multipleStatements: true
});

// Login form
router.get('/login', function(req, res){
    res.render('/login');
  });

export function login() {
    return new Promise((resolve, reject) => {
        let posts, comments, result = [];

        createArrays().then(respons => {
            posts = correctUsers(respons[2], respons[0]);
            comments = correctUsers(respons[2], respons[1]);

            Promise.allSettled([posts, comments]).then(respons2 => {
            respons2.forEach(element => {
                result = [ ... result, element.value ];
            });
            resolve(result);
            });
        });
    })
    
}
  
function createArrays() {
    return new Promise((resolve, reject) => {
        let posts, comments, users, result = [];
      
        posts = getPosts();
        comments = getComments();
        users = getUsers();
  
        Promise.allSettled([posts, comments, users]).then(res => {
            res.forEach(element => {
            result = [ ... result, element.value ];
            });
            resolve(result);
        });
    });  
}
  
function correctUsers(users, array) {
    return new Promise((resolve, reject) => {
        for (let u = 0; users[u]; u++) {
            for (let a = 0; array[a]; a++) {
            if (array[a].user == users[u].uid) {
                array[a].user = users[u].email;
            }
            }
        }
        resolve(array);
    });  
}
  
function getUsers() {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM users', (err, result) => {
            if (err) {
            console.log("Error in database operation, posts");
            } else {
            resolve(result);
            }
        }); 
    });  
}
  
function getPosts() {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM posts', (err, result) => {
            if (err) {
            console.log("Error in database operation, posts");
            } else {
            resolve(result);
            }
        }); 
    });
}
  
function getComments() {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM comments', (err, result) => {
            if (err) {
            console.log("Error in database operation, posts");
            } else {
            resolve(result);
            }
        });  
    });
}